#ifndef EFFECTSNAME_H
#define EFFECTSNAME_H

#include <string>

struct effect_names
{
    std::string classname;      //Internal Name reference for mapping
    std::string ui_name;        //User friendly name (Untranslated)
};

#endif // EFFECTSNAME_H
