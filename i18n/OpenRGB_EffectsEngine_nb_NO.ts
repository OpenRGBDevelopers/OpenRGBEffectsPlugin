<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>Ambient</name>
    <message>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <source>Select rectangle...</source>
        <translation>Velg rektangel...</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Venstre</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Top</translation>
    </message>
    <message>
        <source>Width</source>
        <translation>Bredde</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Høyde</translation>
    </message>
    <message>
        <source>Smoothness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screen</source>
        <translation>Skjerm</translation>
    </message>
    <message>
        <source>Takes a portion of the screen and reflect it to your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scaled average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screen copy</source>
        <translation>Skjermkopi</translation>
    </message>
    <message>
        <source>Ambient</source>
        <translation>Ambient</translation>
    </message>
    <message>
        <source>Framerate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select screen</source>
        <translation>Velg skjerm</translation>
    </message>
</context>
<context>
    <name>AudioParty</name>
    <message>
        <source>Zones</source>
        <translation>Soner</translation>
    </message>
    <message>
        <source>Effect threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Divisions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation>Lydinnstillinger</translation>
    </message>
    <message>
        <source>Movement, color change, effects according to audio zones.&lt;br/&gt;Blue zone: motion&lt;br/&gt;Green zone: colors&lt;br/&gt;Red zone: effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color change speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Party</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioSettings</name>
    <message>
        <source>Equalizer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Capture settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset EQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Normalization offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Amplitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Filter constant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Normalization scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Decay (% per step)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AudioDevice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FFT Window mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Average mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Average size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restore default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioSine</name>
    <message>
        <source>Repeat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Glow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cycle speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Oscillation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wave color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sinusoidal audio rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spectrum cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Static</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Sine</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioStar</name>
    <message>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Beat Sensivity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edge beat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star audio visualizer (frequency based) with an edge beat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Star</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioSync</name>
    <message>
        <source>Hue shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color fade speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Roll mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Silent color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rendering options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Band-pass filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display frequency based colors with different modes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saturate high amplitudes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Black and white mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Linear horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No roll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Linear vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioVUMeter</name>
    <message>
        <source>Hue offset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invert hue direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hue spread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill your led strip based on audio load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio VU Meter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioVisualizer</name>
    <message>
        <source>Display audio equalizer on your devices. A ported version of &lt;a href=&quot;https://gitlab.com/CalcProgrammer1/KeyboardVisualizer&quot;&gt;KeyboardVisualizer&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Visualizer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioVisualizerUi</name>
    <message>
        <source>Rendering options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Animation Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Foreground Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Single Color Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reactive Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Silent Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Bloom</name>
    <message>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Flower blooming effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bloom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BouncingBall</name>
    <message>
        <source>Drop Height %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spectrum Velocity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal Velocity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>How fast the ball moves side to side</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ball Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gravity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A ball bounces around your RGB setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bouncing Ball</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Breathing</name>
    <message>
        <source>Fading in and out user selected colors across an entire zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Breathing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BreathingCircle</name>
    <message>
        <source>A breathing circle effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Breathing Circle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Bubbles</name>
    <message>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Max bubbles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rarity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Max expansion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bubbles thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bloop bloop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bubbles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Clock</name>
    <message>
        <source>Clock mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Digital Clock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>12-hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>24-hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorUtils</name>
    <message>
        <source>Multiply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overlay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dodge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Burn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lighten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Darken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exclusive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Difference</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorWheel</name>
    <message>
        <source>Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A rotating rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counter-clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color Wheel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorsPicker</name>
    <message>
        <source>Colors count</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Comet</name>
    <message>
        <source>A comet that travels through your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Comet size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Comet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CrossingBeams</name>
    <message>
        <source>Vertical speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Glow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Two beams that move horizontally and vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Crossing Beams</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomBlink</name>
    <message>
        <source>Clear list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current pattern:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Make your own blinking sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Blink</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomGradientWave</name>
    <message>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create your own gradient wave or use predefined color set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radial out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radial in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unicorn Vomit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Borealis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ocean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pink/Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pink/Gold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pulse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Purple/Orange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>LightBlue/Purple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Police Beacon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Seabed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sunset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vaporwave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Gradient Wave</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomMarquee</name>
    <message>
        <source>Create your own marquee effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Marquee</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceList</name>
    <message>
        <source>Toggle brightness sliders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>Velg alle</translation>
    </message>
    <message>
        <source>Reverse all</source>
        <translation>Reverser alle</translation>
    </message>
</context>
<context>
    <name>DeviceListItem</name>
    <message>
        <source>This device doesn&apos;t have direct mode
Using an effect on a device WILL damage the flash or controller</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add to current effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change direction</source>
        <translation>Bytt retning</translation>
    </message>
</context>
<context>
    <name>DoubleRotatingRainbow</name>
    <message>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Two rainbows that rotate synchronously</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Double Rotating Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EffectList</name>
    <message>
        <source>Start/Stop all effects</source>
        <translation>Start/stop alle effekter</translation>
    </message>
    <message>
        <source>Effects...</source>
        <translation>Effekter...</translation>
    </message>
</context>
<context>
    <name>EffectSearch</name>
    <message>
        <source>No results match</source>
        <translation>Ingen samsvarende resultater</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Søk</translation>
    </message>
</context>
<context>
    <name>EffectTabHeader</name>
    <message>
        <source>Rename effect</source>
        <translation>Bytt effektnavn</translation>
    </message>
    <message>
        <source>Remove effect</source>
        <translation>Fjern effekt</translation>
    </message>
    <message>
        <source>New name:</source>
        <translation>Nytt navn:</translation>
    </message>
</context>
<context>
    <name>Fill</name>
    <message>
        <source>Progressivly fills your devices with a defined color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Fyll</translation>
    </message>
</context>
<context>
    <name>FractalMotion</name>
    <message>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Amplitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Freq m8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Psychedelic sinusoid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fractal Motion</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GLSLCodeEditor</name>
    <message>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>#version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tab 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tab 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>110</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shader editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Main shader</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GifPlayer</name>
    <message>
        <source>Choose GIF file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open GIF file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GIF Files (*.gif)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use GIFs to create your own effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gif Player</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GlobalSettings</name>
    <message>
        <source>Device settings:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configure screen recorder behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set default values for new effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configure devices behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ambient settings:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide devices without Direct mode (restart required)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio settings:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Amount of screenshot taken by the ScreenRecorder engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FPS capture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set default values for audio effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Always use random colors when supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use prefered colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Effects settings:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Hypnotoad</name>
    <message>
        <source>Spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Animation speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Animation direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rotation direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rotation speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You wont escape this</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counter-clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To the inside</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To the outside</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hypnotoad</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LayerEntry</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit layer settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LayerGroupEntry</name>
    <message>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Group composer function</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Layers</name>
    <message>
        <source>Tab 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tab 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combine effects together.&lt;br /&gt;&lt;a href=&quot;https://en.wikipedia.org/wiki/Blend_modes&quot;&gt;Help about blend modes&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combine multiple effects within a group, and combine groups together</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Lightning</name>
    <message>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prepare yourself for thunderstorm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lightning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LivePreviewController</name>
    <message>
        <source>Reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scale content</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Marquee</name>
    <message>
        <source>A simple marquee for your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Marquee</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Mask</name>
    <message>
        <source>X axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invert colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A simple mask for using in layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Mosaic</name>
    <message>
        <source>Rarity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tiles randomly spawning across your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mosaic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MotionPoint</name>
    <message>
        <source>Background color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A point that moves forth and back on your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Motion Point</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MotionPoints</name>
    <message>
        <source>Multiple points that moves in all directions on your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Motion Points</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MovingPanes</name>
    <message>
        <source>Parts of your devices in symmetrical motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Divisions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Moving Panes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewShaderPassTabHeader</name>
    <message>
        <source>+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Buffer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NoiseMap</name>
    <message>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Octaves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Motion speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Persistence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lacunarity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Amplitude</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Floor is lava</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inverse rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lava</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Borealis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ocean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chemicals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Noise Map</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenRGBEffectPage</name>
    <message>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>EffectName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Patterns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;EffectDescription &lt;br/&gt;on multiple&lt;br/&gt;Lines&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Slider2Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>First color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expand/Collapse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Colors settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenRGBEffectTab</name>
    <message>
        <source>Profiles</source>
        <translation>Profiler</translation>
    </message>
    <message>
        <source>Load profile</source>
        <translation>Bruk profil</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Lagre</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Slett</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source>No effects added yet.
 Please select one from the list to get started.</source>
        <translation>Ingen effekter lagt til enda.
Vennligst velg en fra listen for å starte.</translation>
    </message>
</context>
<context>
    <name>PluginInfo</name>
    <message>
        <source>Download lastest build</source>
        <translation>Last ned nyeste build</translation>
    </message>
    <message>
        <source>Git branch:</source>
        <translation>Git branch:</translation>
    </message>
    <message>
        <source>Git commit date:</source>
        <translation>Git commit dato:</translation>
    </message>
    <message>
        <source>Git commit ID:</source>
        <translation>Git commit ID:</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>Versjon:</translation>
    </message>
    <message>
        <source>Build date:</source>
        <translation>Build dato:</translation>
    </message>
    <message>
        <source>Documentation:</source>
        <translation>Dokumentasjon:</translation>
    </message>
    <message>
        <source>&lt;a href=&quot;https://gitlab.com/OpenRGBDevelopers/OpenRGB-Wiki/-/blob/stable/Plugins/Effects/Effects.md&quot;&gt;help&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open plugin folder</source>
        <translation>Åpne pluginmappen</translation>
    </message>
</context>
<context>
    <name>RGBEffect</name>
    <message>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Beams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Simple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Special</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RadialRainbow</name>
    <message>
        <source>X position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dive into the RGB tunnel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Squares</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radial Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Rain</name>
    <message>
        <source>Drop Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Droplet effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drops</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rain</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RainbowWave</name>
    <message>
        <source>A sliding Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rainbow Wave</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RandomMarquee</name>
    <message>
        <source>A simple Random Marquee for your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random Marquee</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RandomSpin</name>
    <message>
        <source>A simple Random Spin for your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random Spin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RotatingBeam</name>
    <message>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A beam that rotates in different ways</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Glow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counter clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pendulum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wipers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swing H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swing V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rotating Beam</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RotatingRainbow</name>
    <message>
        <source>Color speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A rainbow that rotates around the center of your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rotating Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveProfilePopup</name>
    <message>
        <source>Save effects state</source>
        <translation>Lagre effekttilstand</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Lagre</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <source>Or create a new one:</source>
        <translation>Eller lag en ny:</translation>
    </message>
    <message>
        <source>Load profile at startup</source>
        <translation>Bruk profil ved pålogging</translation>
    </message>
    <message>
        <source>Choose an existing profile:</source>
        <translation>Velg en eksisterende profil:</translation>
    </message>
    <message>
        <source>Enter a profile name:</source>
        <translation>Oppgi profilnavn:</translation>
    </message>
    <message>
        <source>Save profile</source>
        <translation>Lagre profil</translation>
    </message>
</context>
<context>
    <name>Sequence</name>
    <message>
        <source>Alternates colors with a fade effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fade time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShaderFileTabHeader</name>
    <message>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShaderPassEditor</name>
    <message>
        <source>The audio data will be automatically passed to this shader. Make sure to enabled &quot;Use audio&quot; in the effect page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Shaders</name>
    <message>
        <source>Save shader as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit shader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invert time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open shaders folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unleash the power of OpenRGB with GL shaders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save shader to file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose a filename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>my-shader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overwrite existing shader:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Or create a new one:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shaders</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmoothBlink</name>
    <message>
        <source>Rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Interval (s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Strength (%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pulse duration (s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pulses (n)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create your own breathing sequences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Smooth Blink</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpectrumCycling</name>
    <message>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Goes through every solid color of the rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spectrum Cycling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Spiral</name>
    <message>
        <source>Draws a hypnotic spiral on your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiral shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiral</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Stack</name>
    <message>
        <source>Matrix zone direction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fills and stack your devices with a solid color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stack</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StarryNight</name>
    <message>
        <source>Selects a random LED and fades it in an out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starry Night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star fade in speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star on time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star density</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star fade out speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Sunrise</name>
    <message>
        <source>Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Intensity speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Run only once</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Intensity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Grow speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sunrise / Sunset effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sunrise</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Swap</name>
    <message>
        <source>Alternate two colors on your devices from left to right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SwirlCircles</name>
    <message>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rotating circles around the center of your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Glow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swirl Circles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SwirlCirclesAudio</name>
    <message>
        <source>Audio settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rotating circles reacting to audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Glow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swirl Circles Audio</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Visor</name>
    <message>
        <source>A back and forth effect motion, flipping colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Visor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Wavy</name>
    <message>
        <source>Will affect how many waves you will see</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wave frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Will affect the wave speed (left to right)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wave speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Will affect the wave duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Oscillation speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alternate colors like waves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wavy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ZigZag</name>
    <message>
        <source>A snake moving on your matrix typed devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ZigZag</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ZoneListItem</name>
    <message>
        <source>Add to current effect</source>
        <translation>Legg til gjeldende effekt</translation>
    </message>
    <message>
        <source>Change direction</source>
        <translation>Bytt retning</translation>
    </message>
</context>
</TS>
