<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ms_MY">
<context>
    <name>Ambient</name>
    <message>
        <source>Mode</source>
        <translation>Mod</translation>
    </message>
    <message>
        <source>Select rectangle...</source>
        <translation>Pilih segi empat tepat...</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Kiri</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Atas</translation>
    </message>
    <message>
        <source>Width</source>
        <translation>Lebar</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Ketinggian</translation>
    </message>
    <message>
        <source>Smoothness</source>
        <translation>Kelancaran</translation>
    </message>
    <message>
        <source>Screen</source>
        <translation>Skrin</translation>
    </message>
    <message>
        <source>Takes a portion of the screen and reflect it to your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scaled average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screen copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ambient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Framerate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Crop stream</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioParty</name>
    <message>
        <source>Zones</source>
        <translation>Zon</translation>
    </message>
    <message>
        <source>Effect threshold</source>
        <translation>Ambang kesan</translation>
    </message>
    <message>
        <source>Divisions</source>
        <translation>Bahagian</translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation>Tetapan audio</translation>
    </message>
    <message>
        <source>Movement, color change, effects according to audio zones.&lt;br/&gt;Blue zone: motion&lt;br/&gt;Green zone: colors&lt;br/&gt;Red zone: effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color change speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Party</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioSettings</name>
    <message>
        <source>Equalizer</source>
        <translation>Penyamaan</translation>
    </message>
    <message>
        <source>Capture settings</source>
        <translation>Tetapan tangkapan</translation>
    </message>
    <message>
        <source>Reset EQ</source>
        <translation>Tetapkan Semula</translation>
    </message>
    <message>
        <source>Normalization offset</source>
        <translation>Pengimbangan normalisasi</translation>
    </message>
    <message>
        <source>Amplitude</source>
        <translation>Amplitud</translation>
    </message>
    <message>
        <source>Filter constant</source>
        <translation>Pemalar penapis</translation>
    </message>
    <message>
        <source>Normalization scale</source>
        <translation>Skala Normalisasi</translation>
    </message>
    <message>
        <source>Decay (% per step)</source>
        <translation>Pereputan (% setiap langkah)</translation>
    </message>
    <message>
        <source>AudioDevice</source>
        <translation>Peranti audio</translation>
    </message>
    <message>
        <source>FFT Window mode</source>
        <translation>Mod Tetingkap FFT</translation>
    </message>
    <message>
        <source>Average mode</source>
        <translation>Mod purata</translation>
    </message>
    <message>
        <source>Average size</source>
        <translation>Saiz purata</translation>
    </message>
    <message>
        <source>Restore default</source>
        <translation>Pulihkan lalai</translation>
    </message>
</context>
<context>
    <name>AudioSine</name>
    <message>
        <source>Repeat</source>
        <translation>Ulangi</translation>
    </message>
    <message>
        <source>Glow</source>
        <translation>Bercahaya</translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation>Tetapan audio</translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation>Ketebalan</translation>
    </message>
    <message>
        <source>Color mode</source>
        <translation>Mod warna</translation>
    </message>
    <message>
        <source>Background</source>
        <translation>Latar</translation>
    </message>
    <message>
        <source>Cycle speed</source>
        <translation>Kelajuan kitaran</translation>
    </message>
    <message>
        <source>Oscillation</source>
        <translation>Getaran</translation>
    </message>
    <message>
        <source>Wave color</source>
        <translation>Warna gelombang</translation>
    </message>
    <message>
        <source>Sinusoidal audio rendering</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spectrum cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Static</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Sine</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioStar</name>
    <message>
        <source>Saturation</source>
        <translation>Ketepuan</translation>
    </message>
    <message>
        <source>Hue</source>
        <translation>Hue</translation>
    </message>
    <message>
        <source>Beat Sensivity</source>
        <translation>Beat Sensitiviti</translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation>Tetapan audio</translation>
    </message>
    <message>
        <source>Edge beat</source>
        <translation>Beat tepi</translation>
    </message>
    <message>
        <source>Star audio visualizer (frequency based) with an edge beat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Star</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioSync</name>
    <message>
        <source>Hue shift</source>
        <translation>Peralihan Hue</translation>
    </message>
    <message>
        <source>Color fade speed</source>
        <translation>Kelajuan pudar warna</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation>Ketepuan</translation>
    </message>
    <message>
        <source>Roll mode</source>
        <translation>Mod gulung</translation>
    </message>
    <message>
        <source>Silent color</source>
        <translation>Warna senyap</translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation>Tetapan audio</translation>
    </message>
    <message>
        <source>Rendering options</source>
        <translation>Pilihan rendering</translation>
    </message>
    <message>
        <source>Band-pass filter</source>
        <translation>Penapis laluan jalur</translation>
    </message>
    <message>
        <source>Display frequency based colors with different modes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saturate high amplitudes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Black and white mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Linear horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No roll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Linear vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioVUMeter</name>
    <message>
        <source>Hue offset</source>
        <translation>Hue mengimbangi</translation>
    </message>
    <message>
        <source>Invert hue direction</source>
        <translation>Terbalikkan arah rona</translation>
    </message>
    <message>
        <source>Hue spread</source>
        <translation>Hue tersebar</translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation>Tetapan audio</translation>
    </message>
    <message>
        <source>Saturation</source>
        <translation>Ketepuan</translation>
    </message>
    <message>
        <source>Fill your led strip based on audio load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio VU Meter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioVisualizer</name>
    <message>
        <source>Display audio equalizer on your devices. A ported version of &lt;a href=&quot;https://gitlab.com/CalcProgrammer1/KeyboardVisualizer&quot;&gt;KeyboardVisualizer&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio Visualizer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AudioVisualizerUi</name>
    <message>
        <source>Rendering options</source>
        <translation>Pilihan rendering</translation>
    </message>
    <message>
        <source>Background Brightness</source>
        <translation>Kecerahan Latar</translation>
    </message>
    <message>
        <source>Animation Speed</source>
        <translation>Kelajuan Animasi</translation>
    </message>
    <message>
        <source>Background Mode</source>
        <translation>Mod Latar Belakang</translation>
    </message>
    <message>
        <source>Foreground Mode</source>
        <translation>Mod Latar Depan</translation>
    </message>
    <message>
        <source>Single Color Mode</source>
        <translation>Mod Warna Tunggal</translation>
    </message>
    <message>
        <source>Background Timeout</source>
        <translation>Tamat Masa Latar</translation>
    </message>
    <message>
        <source>Reactive Background</source>
        <translation>Latar Belakang Reaktif</translation>
    </message>
    <message>
        <source>Silent Background</source>
        <translation>Latar Belakang Senyap</translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation>Tetapan audio</translation>
    </message>
</context>
<context>
    <name>Bloom</name>
    <message>
        <source>Saturation</source>
        <translation>Ketepuan</translation>
    </message>
    <message>
        <source>Flower blooming effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bloom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BouncingBall</name>
    <message>
        <source>Drop Height %</source>
        <translation>Ketinggian Jatuh %</translation>
    </message>
    <message>
        <source>Spectrum Velocity</source>
        <translation>Halaju Spektrum</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation>Lalai</translation>
    </message>
    <message>
        <source>Horizontal Velocity</source>
        <translation>Halaju Mendatar</translation>
    </message>
    <message>
        <source>How fast the ball moves side to side</source>
        <translation>Berapa laju bola bergerak dari sisi ke sisi</translation>
    </message>
    <message>
        <source>Ball Radius</source>
        <translation>Jejari Bola</translation>
    </message>
    <message>
        <source>Gravity</source>
        <translation>Graviti</translation>
    </message>
    <message>
        <source>A ball bounces around your RGB setup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>-</source>
        <translation type="unfinished">-</translation>
    </message>
    <message>
        <source>Bouncing Ball</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Breathing</name>
    <message>
        <source>Fading in and out user selected colors across an entire zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Breathing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BreathingCircle</name>
    <message>
        <source>A breathing circle effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation type="unfinished">Ketebalan</translation>
    </message>
    <message>
        <source>Breathing Circle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Bubbles</name>
    <message>
        <source>Speed</source>
        <translation>Kelajuan</translation>
    </message>
    <message>
        <source>Max bubbles</source>
        <translation>Gelembung Maks</translation>
    </message>
    <message>
        <source>Rarity</source>
        <translation>Rarity</translation>
    </message>
    <message>
        <source>Background</source>
        <translation>Latar</translation>
    </message>
    <message>
        <source>Max expansion</source>
        <translation>Pengembangan maks</translation>
    </message>
    <message>
        <source>Bubbles thickness</source>
        <translation>Ketebalan gelembung</translation>
    </message>
    <message>
        <source>Bloop bloop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bubbles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Clock</name>
    <message>
        <source>Clock mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Digital Clock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>12-hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>24-hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorUtils</name>
    <message>
        <source>Multiply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Screen</source>
        <translation type="unfinished">Skrin</translation>
    </message>
    <message>
        <source>Overlay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dodge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Burn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lighten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Darken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exclusive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Difference</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorWheel</name>
    <message>
        <source>Direction</source>
        <translation type="unfinished">Arah</translation>
    </message>
    <message>
        <source>X position</source>
        <translation type="unfinished">Kedudukan X</translation>
    </message>
    <message>
        <source>Y position</source>
        <translation type="unfinished">Kedudukan Y</translation>
    </message>
    <message>
        <source>A rotating rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counter-clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color Wheel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorsPicker</name>
    <message>
        <source>Colors count</source>
        <translation>Kiraan warna</translation>
    </message>
</context>
<context>
    <name>Comet</name>
    <message>
        <source>A comet that travels through your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Comet size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Comet</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CrossingBeams</name>
    <message>
        <source>Vertical speed</source>
        <translation>Kelajuan menegak</translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation>Ketebalan</translation>
    </message>
    <message>
        <source>Glow</source>
        <translation>Bercahaya</translation>
    </message>
    <message>
        <source>Horizontal speed</source>
        <translation>Kelajuan mendatar</translation>
    </message>
    <message>
        <source>Two beams that move horizontally and vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Crossing Beams</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomBlink</name>
    <message>
        <source>Clear list</source>
        <translation>Kosongkan senarai</translation>
    </message>
    <message>
        <source>Interval</source>
        <translation>Selang</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Tambah</translation>
    </message>
    <message>
        <source>Current pattern:</source>
        <translation>Corak semasa:</translation>
    </message>
    <message>
        <source>Reset time</source>
        <translation>Set semula masa</translation>
    </message>
    <message>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Buang yang ditanda</translation>
    </message>
    <message>
        <source>Make your own blinking sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Blink</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomGradientWave</name>
    <message>
        <source>Height</source>
        <translation>Ketinggian</translation>
    </message>
    <message>
        <source>Preset</source>
        <translation>Pratetap</translation>
    </message>
    <message>
        <source>Spread</source>
        <translation>Sebar</translation>
    </message>
    <message>
        <source>Direction</source>
        <translation>Arah</translation>
    </message>
    <message>
        <source>Width</source>
        <translation>Lebar</translation>
    </message>
    <message>
        <source>Create your own gradient wave or use predefined color set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radial out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radial in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unicorn Vomit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Borealis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ocean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pink/Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pink/Gold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pulse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Purple/Orange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>LightBlue/Purple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Police Beacon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Seabed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sunset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vaporwave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Gradient Wave</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomMarquee</name>
    <message>
        <source>Create your own marquee effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Marquee</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceList</name>
    <message>
        <source>Toggle brightness sliders</source>
        <translation>Togol peluncur kecerahan</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>Pilih semua</translation>
    </message>
    <message>
        <source>Reverse all</source>
        <translation>Balikkan semua</translation>
    </message>
</context>
<context>
    <name>DeviceListItem</name>
    <message>
        <source>This device doesn&apos;t have direct mode
Using an effect on a device WILL damage the flash or controller</source>
        <translation>Peranti ini tiada mod direct
Menggunakan kesan pada peranti AKAN merosakkan flash atau pengawal</translation>
    </message>
    <message>
        <source>Add to current effect</source>
        <translation>Tambahkan pada kesan semasa</translation>
    </message>
    <message>
        <source>Change direction</source>
        <translation>Tukar arah</translation>
    </message>
</context>
<context>
    <name>DoubleRotatingRainbow</name>
    <message>
        <source>Frequency</source>
        <translation>Kekerapan</translation>
    </message>
    <message>
        <source>Two rainbows that rotate synchronously</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Double Rotating Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EffectList</name>
    <message>
        <source>Start/Stop all effects</source>
        <translation>Mula/Hentikan semua kesan</translation>
    </message>
    <message>
        <source>Effects...</source>
        <translation>Kesan...</translation>
    </message>
</context>
<context>
    <name>EffectSearch</name>
    <message>
        <source>No results match</source>
        <translation>Tiada hasil yang sepadan</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Cari</translation>
    </message>
</context>
<context>
    <name>EffectTabHeader</name>
    <message>
        <source>Rename effect</source>
        <translation>Namakan semula kesan</translation>
    </message>
    <message>
        <source>Remove effect</source>
        <translation>Padam kesan</translation>
    </message>
    <message>
        <source>New name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fill</name>
    <message>
        <source>Progressivly fills your devices with a defined color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FractalMotion</name>
    <message>
        <source>Thickness</source>
        <translation>Ketebalan</translation>
    </message>
    <message>
        <source>Freq m10</source>
        <translation>Kekerapan m10</translation>
    </message>
    <message>
        <source>Freq m12</source>
        <translation>Kekerapan m12</translation>
    </message>
    <message>
        <source>Freq m2</source>
        <translation>Kekerapan m2</translation>
    </message>
    <message>
        <source>Amplitude</source>
        <translation>Amplitud</translation>
    </message>
    <message>
        <source>Freq m5</source>
        <translation>Kekerapan m5</translation>
    </message>
    <message>
        <source>Background color:</source>
        <translation>Warna Latar:</translation>
    </message>
    <message>
        <source>Freq m6</source>
        <translation>Kekerapan m6</translation>
    </message>
    <message>
        <source>Freq m1</source>
        <translation>Kekerapan m1</translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation>Kekerapan</translation>
    </message>
    <message>
        <source>Freq m3</source>
        <translation>Kekerapan m3</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation>Lalai</translation>
    </message>
    <message>
        <source>Freq m7</source>
        <translation>Kekerapan m7</translation>
    </message>
    <message>
        <source>Freq m9</source>
        <translation>Kekerapan m9</translation>
    </message>
    <message>
        <source>Freq m4</source>
        <translation>Kekerapan m4</translation>
    </message>
    <message>
        <source>Freq m11</source>
        <translation>Kekerapan m11</translation>
    </message>
    <message>
        <source>Freq m8</source>
        <translation>Kekerapan m8</translation>
    </message>
    <message>
        <source>Psychedelic sinusoid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fractal Motion</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GLSLCodeEditor</name>
    <message>
        <source>Style</source>
        <translation>Gaya</translation>
    </message>
    <message>
        <source>#version </source>
        <translation>#versi </translation>
    </message>
    <message>
        <source>Tab 1</source>
        <translation>Tab 1</translation>
    </message>
    <message>
        <source>Tab 2</source>
        <translation>Tab 2</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Gunakan</translation>
    </message>
    <message>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <source>110</source>
        <translation>110</translation>
    </message>
    <message>
        <source>Shader editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Main shader</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GifPlayer</name>
    <message>
        <source>Choose GIF file</source>
        <translation>Pilih fail GIF</translation>
    </message>
    <message>
        <source>Open GIF file</source>
        <translation>Buka fail GIF</translation>
    </message>
    <message>
        <source>GIF Files (*.gif)</source>
        <translation>Fail GIF (*.gif)</translation>
    </message>
    <message>
        <source>Use GIFs to create your own effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gif Player</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GlobalSettings</name>
    <message>
        <source>Device settings:</source>
        <translation>Tetapan peranti:</translation>
    </message>
    <message>
        <source>Configure screen recorder behavior</source>
        <translation>Konfigurasikan tingkah laku perakam skrin</translation>
    </message>
    <message>
        <source>Set default values for new effects</source>
        <translation>Tetapkan nilai lalai untuk kesan baharu</translation>
    </message>
    <message>
        <source>Configure devices behavior</source>
        <translation>Konfigurasikan tingkah laku peranti</translation>
    </message>
    <message>
        <source>Ambient settings:</source>
        <translation>Tetapan ambien:</translation>
    </message>
    <message>
        <source>Hide devices without Direct mode (restart required)</source>
        <translation>Sembunyikan peranti tanpa mod &apos;Direct&apos; (diperlukan restart)</translation>
    </message>
    <message>
        <source>Audio settings:</source>
        <translation>Tetapan audio:</translation>
    </message>
    <message>
        <source>Amount of screenshot taken by the ScreenRecorder engine</source>
        <translation>Jumlah tangkapan skrin yang diambil oleh enjin Perakam Skrin</translation>
    </message>
    <message>
        <source>FPS capture</source>
        <translation>Tangkapan FPS</translation>
    </message>
    <message>
        <source>Set default values for audio effects</source>
        <translation>Tetapkan nilai lalai untuk kesan audio</translation>
    </message>
    <message>
        <source>FPS</source>
        <translation>FPS</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation>Kecerahan</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Suhu</translation>
    </message>
    <message>
        <source>Tint</source>
        <translation>Tint</translation>
    </message>
    <message>
        <source>Always use random colors when supported</source>
        <translation>Sentiasa menggunakan warna rawak apabila disokong</translation>
    </message>
    <message>
        <source>Use prefered colors</source>
        <translation>Gunakan warna yang diutamakan</translation>
    </message>
    <message>
        <source>Effects settings:</source>
        <translation>Tetapan kesan:</translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation>Tetapan audio</translation>
    </message>
</context>
<context>
    <name>Hypnotoad</name>
    <message>
        <source>Spacing</source>
        <translation>Jarak</translation>
    </message>
    <message>
        <source>Animation speed</source>
        <translation>Kelajuan Animasi</translation>
    </message>
    <message>
        <source>Animation direction</source>
        <translation>Arah animasi</translation>
    </message>
    <message>
        <source>Color mode</source>
        <translation>Mod warna</translation>
    </message>
    <message>
        <source>X position</source>
        <translation>Kedudukan X</translation>
    </message>
    <message>
        <source>Y position</source>
        <translation>Kedudukan Y</translation>
    </message>
    <message>
        <source>Rotation direction</source>
        <translation>Arah putaran</translation>
    </message>
    <message>
        <source>Rotation speed</source>
        <translation>Kelajuan putaran</translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation>Ketebalan</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">TextLabel</translation>
    </message>
    <message>
        <source>You wont escape this</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counter-clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To the inside</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To the outside</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hypnotoad</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LayerEntry</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Form</translation>
    </message>
    <message>
        <source>Edit layer settings</source>
        <translation>Edit tetapan lapisan</translation>
    </message>
    <message>
        <source>Remove layer</source>
        <translation>Padam lapisan</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">TextLabel</translation>
    </message>
</context>
<context>
    <name>LayerGroupEntry</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Form</translation>
    </message>
    <message>
        <source>Delete group</source>
        <translation>Padam kumpulan</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Jelas</translation>
    </message>
    <message>
        <source>Group composer function</source>
        <translation>Fungsi komposer kumpulan</translation>
    </message>
</context>
<context>
    <name>Layers</name>
    <message>
        <source>Tab 1</source>
        <translation>Tab 1</translation>
    </message>
    <message>
        <source>Tab 2</source>
        <translation>Tab 2</translation>
    </message>
    <message>
        <source>Combine effects together.&lt;br /&gt;&lt;a href=&quot;https://en.wikipedia.org/wiki/Blend_modes&quot;&gt;Help about blend modes&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combine multiple effects within a group, and combine groups together</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Lightning</name>
    <message>
        <source>Mode</source>
        <translation>Mod</translation>
    </message>
    <message>
        <source>Prepare yourself for thunderstorm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lightning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LivePreviewController</name>
    <message>
        <source>Reverse</source>
        <translation>Terbalik</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation>Kecerahan</translation>
    </message>
    <message>
        <source>Custom height</source>
        <translation>Ketinggian tersuai</translation>
    </message>
    <message>
        <source>Custom width</source>
        <translation>Lebar tersuai</translation>
    </message>
    <message>
        <source>Scale content</source>
        <translation>Skala kandungan</translation>
    </message>
</context>
<context>
    <name>Marquee</name>
    <message>
        <source>A simple marquee for your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spacing</source>
        <translation type="unfinished">Jarak</translation>
    </message>
    <message>
        <source>Marquee</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Mask</name>
    <message>
        <source>X axis</source>
        <translation>Paksi X</translation>
    </message>
    <message>
        <source>Y axis</source>
        <translation>Paksi Y</translation>
    </message>
    <message>
        <source>width</source>
        <translation>Lebar</translation>
    </message>
    <message>
        <source>height</source>
        <translation>Ketinggian</translation>
    </message>
    <message>
        <source>Invert colors</source>
        <translation>Terbalikkan warna</translation>
    </message>
    <message>
        <source>A simple mask for using in layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Mosaic</name>
    <message>
        <source>Rarity</source>
        <translation type="unfinished">Rarity</translation>
    </message>
    <message>
        <source>Tiles randomly spawning across your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mosaic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MotionPoint</name>
    <message>
        <source>Background color:</source>
        <translation>Warna latar belakang:</translation>
    </message>
    <message>
        <source>A point that moves forth and back on your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Motion Point</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MotionPoints</name>
    <message>
        <source>Multiple points that moves in all directions on your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Motion Points</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MovingPanes</name>
    <message>
        <source>Parts of your devices in symmetrical motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Divisions</source>
        <translation type="unfinished">Bahagian</translation>
    </message>
    <message>
        <source>Moving Panes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewShaderPassTabHeader</name>
    <message>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <source>Texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Buffer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NoiseMap</name>
    <message>
        <source>Preset</source>
        <translation>Pratetap</translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation>Kekerapan</translation>
    </message>
    <message>
        <source>Octaves</source>
        <translation>Oktaf</translation>
    </message>
    <message>
        <source>Motion</source>
        <translation>Gerakan</translation>
    </message>
    <message>
        <source>Motion speed</source>
        <translation>Kelajuan gerakan</translation>
    </message>
    <message>
        <source>Persistence</source>
        <translation>Kegigihan</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation>Lalai</translation>
    </message>
    <message>
        <source>Lacunarity</source>
        <translation>Kekosongan</translation>
    </message>
    <message>
        <source>Amplitude</source>
        <translation>Amplitud</translation>
    </message>
    <message>
        <source>Mode</source>
        <translation>Mod</translation>
    </message>
    <message>
        <source>Floor is lava</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Inverse rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="unfinished">Kiri</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lava</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Borealis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ocean</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chemicals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Noise Map</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenRGBEffectPage</name>
    <message>
        <source>Preview</source>
        <translation>Pratonton</translation>
    </message>
    <message>
        <source>EffectName</source>
        <translation>NamaKesan</translation>
    </message>
    <message>
        <source>Patterns</source>
        <translation>Corak</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;EffectDescription &lt;br/&gt;on multiple&lt;br/&gt;Lines&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;PeneranganKesan &lt;br/&gt;pada berbilang&lt;br/&gt;Baris&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <source>Random</source>
        <translation>Rawak</translation>
    </message>
    <message>
        <source>Slider2Label</source>
        <translation>Slider2Label</translation>
    </message>
    <message>
        <source>Temperature</source>
        <translation>Suhu</translation>
    </message>
    <message>
        <source>Tint</source>
        <translation>Tint</translation>
    </message>
    <message>
        <source>First color</source>
        <translation>Warna pertama</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation>Kelajuan</translation>
    </message>
    <message>
        <source>Brightness</source>
        <translation>Kecerahan</translation>
    </message>
    <message>
        <source>FPS</source>
        <translation>FPS</translation>
    </message>
    <message>
        <source>Expand/Collapse</source>
        <translation>Kembangkan/Runtuhkan</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation>Warna</translation>
    </message>
    <message>
        <source>Colors settings</source>
        <translation>Tetapan warna</translation>
    </message>
</context>
<context>
    <name>OpenRGBEffectTab</name>
    <message>
        <source>Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Simpan</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No effects added yet.
 Please select one from the list to get started.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PluginInfo</name>
    <message>
        <source>Download lastest build</source>
        <translation>Muat turun binaan terkini</translation>
    </message>
    <message>
        <source>Git branch:</source>
        <translation>Cawangan Git:</translation>
    </message>
    <message>
        <source>Git commit date:</source>
        <translation>Tarikh komit Git:</translation>
    </message>
    <message>
        <source>Git commit ID:</source>
        <translation>ID komit Git:</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>Versi:</translation>
    </message>
    <message>
        <source>Build date:</source>
        <translation>Tarikh binaan:</translation>
    </message>
    <message>
        <source>Documentation:</source>
        <translation>Dokumentasi:</translation>
    </message>
    <message>
        <source>&lt;a href=&quot;https://gitlab.com/OpenRGBDevelopers/OpenRGB-Wiki/-/blob/stable/Plugins/Effects/Effects.md&quot;&gt;help&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;https://gitlab.com/OpenRGBDevelopers/OpenRGB-Wiki/-/blob/stable/Plugins/Effects/Effects.md&quot;&gt;help&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Open plugin folder</source>
        <translation>Buka folder plugin</translation>
    </message>
</context>
<context>
    <name>RGBEffect</name>
    <message>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Beams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random</source>
        <translation type="unfinished">Rawak</translation>
    </message>
    <message>
        <source>Simple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Special</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RadialRainbow</name>
    <message>
        <source>X position</source>
        <translation>Kedudukan X</translation>
    </message>
    <message>
        <source>Shape</source>
        <translation>Bentuk</translation>
    </message>
    <message>
        <source>Y position</source>
        <translation>Kedudukan Y</translation>
    </message>
    <message>
        <source>Dive into the RGB tunnel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation type="unfinished">Kekerapan</translation>
    </message>
    <message>
        <source>Circles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Squares</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Radial Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Rain</name>
    <message>
        <source>Drop Size</source>
        <translation>Saiz Titisan</translation>
    </message>
    <message>
        <source>Droplet effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drops</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rain</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RainbowWave</name>
    <message>
        <source>A sliding Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation type="unfinished">Kekerapan</translation>
    </message>
    <message>
        <source>Rainbow Wave</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RandomMarquee</name>
    <message>
        <source>A simple Random Marquee for your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random Marquee</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RandomSpin</name>
    <message>
        <source>A simple Random Spin for your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Random Spin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RotatingBeam</name>
    <message>
        <source>Mode</source>
        <translation>Mod</translation>
    </message>
    <message>
        <source>Thickness</source>
        <translation>Ketebalan</translation>
    </message>
    <message>
        <source>A beam that rotates in different ways</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Glow</source>
        <translation type="unfinished">Bercahaya</translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counter clockwise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pendulum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wipers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swing H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swing V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rotating Beam</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RotatingRainbow</name>
    <message>
        <source>Color speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A rainbow that rotates around the center of your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rotating Rainbow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveProfilePopup</name>
    <message>
        <source>Save effects state</source>
        <translation>Simpan keadaan kesan</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Simpan</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <source>Or create a new one:</source>
        <translation>Atau buat yang baharu:</translation>
    </message>
    <message>
        <source>Load profile at startup</source>
        <translation>Muatkan profil semasa permulaan</translation>
    </message>
    <message>
        <source>Choose an existing profile:</source>
        <translation>Pilih profil sedia ada:</translation>
    </message>
    <message>
        <source>Enter a profile name:</source>
        <translation>Masukkan nama profil:</translation>
    </message>
    <message>
        <source>Save profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Sequence</name>
    <message>
        <source>Alternates colors with a fade effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fade time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShaderFileTabHeader</name>
    <message>
        <source>X</source>
        <translation>X</translation>
    </message>
</context>
<context>
    <name>ShaderPassEditor</name>
    <message>
        <source>The audio data will be automatically passed to this shader. Make sure to enabled &quot;Use audio&quot; in the effect page.</source>
        <translation>Data audio akan dihantar secara automatik ke pelorek ini. Pastikan untuk mendayakan &quot;Gunakan audio&quot; dalam halaman kesan.</translation>
    </message>
    <message>
        <source>Choose texture</source>
        <translation>Pilih tekstur</translation>
    </message>
    <message>
        <source>Open Image</source>
        <translation>Buka Imej</translation>
    </message>
    <message>
        <source>Image Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Shaders</name>
    <message>
        <source>Save shader as...</source>
        <translation>Simpan shader sebagai...</translation>
    </message>
    <message>
        <source>Use audio</source>
        <translation>Gunakan audio</translation>
    </message>
    <message>
        <source>Width</source>
        <translation>Lebar</translation>
    </message>
    <message>
        <source>Audio settings</source>
        <translation>Tetapan audio</translation>
    </message>
    <message>
        <source>Edit shader</source>
        <translation>Edit shader</translation>
    </message>
    <message>
        <source>Invert time</source>
        <translation>Terbalikkan masa</translation>
    </message>
    <message>
        <source>Open shaders folder</source>
        <translation>Buka folder shaders</translation>
    </message>
    <message>
        <source>Height</source>
        <translation>Ketinggian</translation>
    </message>
    <message>
        <source>Reset time</source>
        <translation>Set semula masa</translation>
    </message>
    <message>
        <source>Show rendering</source>
        <translation>Tunjukkan rendering</translation>
    </message>
    <message>
        <source>Unleash the power of OpenRGB with GL shaders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save shader to file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose a filename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>my-shader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Overwrite existing shader:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Or create a new one:</source>
        <translation type="unfinished">Atau buat yang baharu:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Batal</translation>
    </message>
    <message>
        <source>Shaders</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmoothBlink</name>
    <message>
        <source>Rendering</source>
        <translation>Rendering</translation>
    </message>
    <message>
        <source>Interval (s)</source>
        <translation>Selang (s)</translation>
    </message>
    <message>
        <source>Strength (%)</source>
        <translation>Kekuatan (%)</translation>
    </message>
    <message>
        <source>Pulse duration (s)</source>
        <translation>Tempoh masa Pulse (s)</translation>
    </message>
    <message>
        <source>Pulses (n)</source>
        <translation>Pulses (n)</translation>
    </message>
    <message>
        <source>Y position</source>
        <translation>Kedudukan Y</translation>
    </message>
    <message>
        <source>X position</source>
        <translation>Kedudukan X</translation>
    </message>
    <message>
        <source>Defaults</source>
        <translation>Lalai</translation>
    </message>
    <message>
        <source>Create your own breathing sequences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Circle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Smooth Blink</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpectrumCycling</name>
    <message>
        <source>Saturation</source>
        <translation>Ketepuan</translation>
    </message>
    <message>
        <source>Goes through every solid color of the rainbow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spectrum Cycling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Spiral</name>
    <message>
        <source>Draws a hypnotic spiral on your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiral shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiral</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Stack</name>
    <message>
        <source>Matrix zone direction</source>
        <translation>Arah zon matriks</translation>
    </message>
    <message>
        <source>Fills and stack your devices with a solid color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Horizontal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vertical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stack</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StarryNight</name>
    <message>
        <source>Selects a random LED and fades it in an out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starry Night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star fade in speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star on time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star density</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Star fade out speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Background brightness</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Sunrise</name>
    <message>
        <source>Motion</source>
        <translation>Gerakan</translation>
    </message>
    <message>
        <source>Intensity speed</source>
        <translation>Kelajuan intensiti</translation>
    </message>
    <message>
        <source>Run only once</source>
        <translation>Berlari hanya sekali</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation>Jejari</translation>
    </message>
    <message>
        <source>Intensity</source>
        <translation>Intensiti</translation>
    </message>
    <message>
        <source>Grow speed</source>
        <translation>Berkembang kelajuan</translation>
    </message>
    <message>
        <source>Sunrise / Sunset effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sunrise</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Swap</name>
    <message>
        <source>Alternate two colors on your devices from left to right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SwirlCircles</name>
    <message>
        <source>Radius</source>
        <translation>Jejari</translation>
    </message>
    <message>
        <source>Rotating circles around the center of your devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Glow</source>
        <translation type="unfinished">Bercahaya</translation>
    </message>
    <message>
        <source>Swirl Circles</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SwirlCirclesAudio</name>
    <message>
        <source>Audio settings</source>
        <translation>Tetapan audio</translation>
    </message>
    <message>
        <source>Radius</source>
        <translation>Jejari</translation>
    </message>
    <message>
        <source>Rotating circles reacting to audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Glow</source>
        <translation type="unfinished">Bercahaya</translation>
    </message>
    <message>
        <source>Swirl Circles Audio</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Visor</name>
    <message>
        <source>A back and forth effect motion, flipping colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="unfinished">Lebar</translation>
    </message>
    <message>
        <source>Visor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Wavy</name>
    <message>
        <source>Will affect how many waves you will see</source>
        <translation>Akan menjejaskan bilangan gelombang yang akan anda lihat</translation>
    </message>
    <message>
        <source>Wave frequency</source>
        <translation>Kekerapan gelombang</translation>
    </message>
    <message>
        <source>Will affect the wave speed (left to right)</source>
        <translation>Akan menjejaskan kelajuan gelombang (kiri ke kanan)</translation>
    </message>
    <message>
        <source>Wave speed</source>
        <translation>Kelajuan gelombang</translation>
    </message>
    <message>
        <source>Will affect the wave duration</source>
        <translation>Akan menjejaskan tempoh gelombang</translation>
    </message>
    <message>
        <source>Oscillation speed</source>
        <translation>Kelajuan ayunan</translation>
    </message>
    <message>
        <source>Alternate colors like waves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wavy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ZigZag</name>
    <message>
        <source>A snake moving on your matrix typed devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ZigZag</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ZoneListItem</name>
    <message>
        <source>Add to current effect</source>
        <translation>Tambahkan pada kesan semasa</translation>
    </message>
    <message>
        <source>Change direction</source>
        <translation>Tukar arah</translation>
    </message>
</context>
</TS>
